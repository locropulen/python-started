## Download docker for mac

https://hub.docker.com/editions/community/docker-ce-desktop-mac/

## Run

```
docker build -t my-python-app .
docker run -it --rm --name my-running-app my-python-app
```

Enjoy!!!
